package org.green.kav.activemq.main;

import org.apache.activemq.advisory.DestinationSource;
import org.apache.activemq.command.ActiveMQQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;
import java.util.Set;

@Component
public class Main implements CommandLineRunner {
    private Logger log = LoggerFactory.getLogger(Main.class);
    @Override
    public void run(String... args) throws Exception {
        String brokerUrl = "tcp://localhost:61616";
        consumeMessage(brokerUrl, "messageBoard");
        getAllOtherQueuesInActiveMq(brokerUrl);
    }

    private void consumeMessage(String brokerUrl, String queueName){
        try {
            ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(brokerUrl);

            //Create Connection
            Connection connection = factory.createConnection();

            // Start the connection
            connection.start();

            // Create Session
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            //Create queue
            Destination queue = session.createQueue(queueName);

            MessageConsumer consumer = session.createConsumer(queue);

            Message message = consumer.receive(1000);

            if (message instanceof TextMessage) {
                TextMessage textMessage = (TextMessage) message;
                String text = textMessage.getText();
                log.info("Consumer Received: {}", text);
            }

            session.close();
            connection.close();
        }
        catch (Exception ex) {
            System.out.println("Exception Occured");
        }
    }

    private void getAllOtherQueuesInActiveMq(String url) {
        log.info("getting all queues");
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
            ActiveMQConnection connection = (ActiveMQConnection) connectionFactory.createConnection();
            connection.start();

            DestinationSource ds = connection.getDestinationSource();


            Set<ActiveMQQueue> queues = ds.getQueues();
            Thread.sleep(10000l); // this seems to ensure that all the queue is found
            log.info("Queue count {}", queues!=null? queues.size(): 0);
            for (ActiveMQQueue activeMQQueue : queues) {
                try {
                    log.info("queue: {}", activeMQQueue.getQueueName());
                    connection.destroyDestination(activeMQQueue);
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
