package org.green.kav.activemq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActivemqConnectorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivemqConnectorApplication.class, args);
	}

}
